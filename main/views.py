from django.shortcuts import render


def home(request):
    return render(request, 'main/something.html')

def something2(request):
    return render(request, 'main/something2.html')
